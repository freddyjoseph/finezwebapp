﻿using FinezWebApp452mvc.Models;
using FinezWebApp452mvc.Repository.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FinezWebApp452mvc.Models.Context;

namespace FinezWebApp452mvc.Repository
{
    public class FinezAppRepository : Repository<FinezApp, int>, IFinezAppRepository
    {
        public FinezAppRepository(ApplicationDbContext _context) : base(_context)
        {
        }

        public override FinezApp get(int Id)
        {
            return this.getAll().Where(x => x.Id == Id).SingleOrDefault();
        }
    }
}