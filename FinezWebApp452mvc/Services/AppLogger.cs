﻿using FinezWebApp452mvc.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;



namespace FinezWebApp452mvc.Services
{
    public class AppLogger
    {


        string log_file = "";
        public AppLogger(string pageName)
        {
            string log_path = System.Web.HttpContext.Current.Server.MapPath("~") + "\\logs\\";
            Directory.CreateDirectory(log_path);
            log_file = log_path + "" + pageName + "_" + DateTime.Now.ToString("yyyyMMMdd") + ".txt";
        }

        public AppLogger(string folderName,string fileName)
        {
            //string log_path = System.Web.HttpContext.Current.Server.MapPath("~") + "\\"+ folderName + "\\";
            string log_path = System.Web.HttpContext.Current.Server.MapPath(HttpContext.Current.Request.ApplicationPath) + "\\" + folderName + "\\"; 
            //Directory.CreateDirectory(log_path);
            log_file = log_path + "" + fileName + ".txt";
        }

        ~AppLogger()
        {
            //fl.Close();
        }

        public void write(string title, string message)
        {
            try
            {
                File.AppendAllText(log_file, Environment.NewLine + Environment.NewLine);
                File.AppendAllText(log_file, DateTime.Now.ToString("HH:mm:ss") + Environment.NewLine + "===================================");
                File.AppendAllText(log_file, Environment.NewLine + title + Environment.NewLine + "-------------------------------" + Environment.NewLine + message);
            }
            catch (Exception e)
            {

            }
        }

        public void write(Exception ex)
        {
            try
            {
                File.AppendAllText(log_file, Environment.NewLine + Environment.NewLine);
                File.AppendAllText(log_file, new JavaScriptSerializer().Serialize(ex));
                File.AppendAllText(log_file, Environment.NewLine + "=======================================================================================" + Environment.NewLine);
            }
            catch (Exception e)
            {

            }
        }

        public void write(FailureResponseException ex)
        {
            try
            {
                File.AppendAllText(log_file, Environment.NewLine + Environment.NewLine);
                File.AppendAllText(log_file, new JavaScriptSerializer().Serialize(ex));
                File.AppendAllText(log_file, Environment.NewLine + "=======================================================================================" + Environment.NewLine);
            }
            catch (Exception e)
            {

            }
        }

    }
}