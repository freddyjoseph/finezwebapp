﻿using AutoMapper;
using FinezWebApp452mvc.Buisiness;
using FinezWebApp452mvc.Controllers.Base;
using FinezWebApp452mvc.Models;
using FinezWebApp452mvc.Models.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace FinezWebApp452mvc.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ClientController : BaseController
    {
        private ClientManager clientManager { get; set; }
        private BranchManager branchManager { get; set; }
        public ClientController()
        {
            this.clientManager = new ClientManager(this.dbContext);
            this.branchManager = new BranchManager(this.dbContext);
        }
        // GET: Client
        public async Task<ActionResult> Index()
        {
            return View(await this.clientManager.getClients(new SearchClientVM()));
        }

        [HttpGet]
        public async Task<ActionResult> GetClients(SearchClientVM model)
        {
            return PartialView("ClientListPartial", await this.clientManager.getClients(model));
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Add(AddClientViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    clientManager.addClient(model);
                    model = new AddClientViewModel();
                    model.IsAdded = true;
                    model.SuccessMessage = "Client added successfully";
                    ModelState.Clear();

                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Failed to add client. Try again");
            }

            return PartialView("AddPartial", model);
        }

        public async Task<ActionResult> Details(int Id)
        {
            var client = await this.clientManager.getClient(Id);
            if (client != null)
                return View(client);
            else
                return View("Error");

        }

        [HttpPost]
        public async Task<ActionResult> AddBranchAndAdmin(AddBranchVM model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    model = await branchManager.AddBranchAndAdmin(model,HttpContext.GetOwinContext());
                    if(model.IsSuccess)
                        ModelState.Clear();
                    else
                        ModelState.AddModelError("", "Failed to add branch. Try again");
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Failed to add branch. Try again");
            }

            return PartialView("AddBranchPartial", model);
        }

        [HttpGet]
        public async Task<ActionResult> GetBranches(int clientId,string SearchText)
        {
            return PartialView("BranchList",await this.branchManager.GetBranches(clientId, SearchText));
        }
    }
}