﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Owin;
using FinezWebApp452mvc.Models;
using FinezWebApp452mvc.Models.Context;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security.OAuth;
using FinezWebApp452mvc.Providers;
using FinezWebApp452mvc.Models.ViewModels;
using System.Web;

namespace FinezWebApp452mvc
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            // Configure the db context, user manager and signin manager to use a single instance per request
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                Provider = new CookieAuthenticationProvider
                {
                    // Enables the application to validate the security stamp when the user logs in.
                    // This is a security feature which is used when you change a password or add an external login to your account.  
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, ApplicationUser>(
                        validateInterval: TimeSpan.FromMinutes(30),
                        regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager)),
                    OnApplyRedirect= ctx=> {
                        if (!IsAPIRequest(ctx.Request))
                        {
                            ctx.Response.Redirect(ctx.RedirectUri);
                        }
                    }
                }
            });            
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Enables the application to temporarily store user information when they are verifying the second factor in the two-factor authentication process.
            app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));

            // Enables the application to remember the second login verification factor such as phone or email.
            // Once you check this option, your second step of verification during the login process will be remembered on the device where you logged in from.
            // This is similar to the RememberMe option when you log in.
            app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);

            app.UseOAuthAuthorizationServer(new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/api/token"),
                Provider = new AuthorizationServerProvider("self"),
                AccessTokenExpireTimeSpan = TimeSpan.FromHours(12),
                AllowInsecureHttp = true,

            });
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());

            //initial seeding of roles
            seedInitUserData();
        }

        public void seedInitUserData()
        {
            ApplicationDbContext context = ApplicationDbContext.Create();
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            if (!roleManager.RoleExists(Roles.Admin.ToString()))
            {
                roleManager.Create(new IdentityRole(Roles.Admin.ToString()));
            }
            if (!roleManager.RoleExists(Roles.Client.ToString()))
            {
                roleManager.Create(new IdentityRole(Roles.Client.ToString()));
            }
            if (!roleManager.RoleExists(Roles.BranchAdmin.ToString()))
            {
                roleManager.Create(new IdentityRole(Roles.BranchAdmin.ToString()));
            }
            if (!roleManager.RoleExists(Roles.FrontOffice.ToString()))
            {
                roleManager.Create(new IdentityRole(Roles.FrontOffice.ToString()));
            }
            if (!roleManager.RoleExists(Roles.Kitchen.ToString()))
            {
                roleManager.Create(new IdentityRole(Roles.Kitchen.ToString()));
            }
            if (!roleManager.RoleExists(Roles.Accountant.ToString()))
            {
                roleManager.Create(new IdentityRole(Roles.Accountant.ToString()));
            }
            ApplicationUserManager userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));
            if (userManager.FindByName("admin") == null)
            {
                var user = new ApplicationUser { UserName = "admin" };
                var result = userManager.Create(user, "#Admin1");
                if (result.Succeeded)
                {
                    var currentUser = userManager.FindByName(user.UserName);

                    var roleresult = userManager.AddToRole(currentUser.Id, Roles.Admin.ToString());
                }
            }
        }

        private static bool IsAPIRequest(IOwinRequest request)
        {
            string apiPath = VirtualPathUtility.ToAbsolute("~/api/");
            return request.Uri.LocalPath.StartsWith(apiPath);
        }
    }
}