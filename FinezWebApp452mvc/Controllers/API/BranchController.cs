﻿using FinezWebApp452mvc.Buisiness;
using FinezWebApp452mvc.Controllers.Base;
using FinezWebApp452mvc.Exceptions;
using FinezWebApp452mvc.Models.ViewModels;
using FinezWebApp452mvc.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace FinezWebApp452mvc.Controllers.API
{
    public class BranchController : BaseAPIController
    {
        BranchManager branchManager;
        AppLogger appLogger;
        public BranchController()
        {
            this.branchManager = new BranchManager(this.dbContext);
            appLogger = new AppLogger("BranchAPIController");
        }

        [Authorize]
        [HttpPut]
        public async Task<IHttpActionResult> RegisterDevice(RegisterDeviceVM model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await this.branchManager.RegisterDevice(model, HttpContext.Current.GetOwinContext());
                    return Ok();
                }
                else
                {
                    appLogger.write("Register model error", string.Join(",", ModelState.Values.SelectMany(v => v.Errors)));
                    return Content(HttpStatusCode.BadRequest, new FailureResponse() { Message = "Device registration failed", Errors = ModelState.Values.SelectMany(v => v.Errors).Select(y => y.ErrorMessage).ToList() });
                }
                
            }
            catch (FailureResponseException e)
            {
                appLogger.write(e);
                return Content(HttpStatusCode.BadRequest, e.FailureResponse);
            }
            catch (Exception e)
            {
                appLogger.write(e);
                return Content(HttpStatusCode.InternalServerError, "");
            }
        }
    }
}
