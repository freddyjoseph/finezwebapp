﻿using FinezWebApp452mvc.Models.Context;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FinezWebApp452mvc.Controllers.Base
{
    public class BaseController : Controller
    {
        protected ApplicationDbContext dbContext;
        public BaseController()
        {
            dbContext = System.Web.HttpContext.Current.GetOwinContext().Get<ApplicationDbContext>();
        }
    }
}