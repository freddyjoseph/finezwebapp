﻿using FinezWebApp452mvc.Models.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FinezWebApp452mvc.Models
{
    public class FinezApp:Entity<int>
    {
        [Column("name"),Required]
        public string Name { get; set; }
        [Column("price"),Required]
        public float Price { get; set; }
        
    }
}