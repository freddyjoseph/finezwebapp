﻿using FinezWebApp452mvc.Models.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FinezWebApp452mvc.Models
{
    public class Branch:Entity<int>
    {
        [Column("client_id")]
        [ForeignKey("Client")]
        public int ClientId { get; set; }
        [Column("name")]
        [MaxLength(50), Required]
        public string Name { get; set; }
        [Column("branch_code")]
        [MaxLength(50), Required]
        public string BranchCode { get; set; }
        [Column("street")]
        [MaxLength(100), Required]
        public string Street { get; set; }
        [Column("city")]
        [MaxLength(50), Required]
        public string City { get; set; }
        [Column("district")]
        [MaxLength(50), Required]
        public string District { get; set; }
        [Column("state")]
        [MaxLength(50), Required]
        public string State { get; set; }
        [Column("pin_code")]
        [MaxLength(50), Required]
        public string Pincode { get; set; }
        [Column("phone"), DataType(DataType.PhoneNumber), Index(IsUnique = true), MaxLength(50)]
        public string Phone { get; set; }
        [Column("mobile"), Required, DataType(DataType.PhoneNumber), Index(IsUnique = true), MaxLength(50)]
        public string Mobile { get; set; }
        [Column("email"), Required, DataType(DataType.EmailAddress), Index(IsUnique = true), MaxLength(50)]
        public string Email { get; set; }
        [Column("website"),DataType(DataType.Url)]
        public string Website { get; set; }
        [Column("is_main_branch",TypeName ="bit")]
        public bool IsMainBranch { get; set; }
        [Column("status")]
        public int Status { get; set; }
        [Column("expiry_date", TypeName = "datetime2")]
        public DateTime ExpiryDate { get; set; }

        public virtual Client Client { get; set; }
        public virtual ICollection<Device> Devices { get; set; }
        public virtual ICollection<Employee> Employees { get; set; }
    }
}