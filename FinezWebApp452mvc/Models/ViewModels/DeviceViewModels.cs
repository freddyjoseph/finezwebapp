﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FinezWebApp452mvc.Models.ViewModels
{
    public class DeviceVM
    {
        public int Id { get; set; }
        [DisplayName("Name")]
        public string Name { get; set; }
        [DisplayName("ID1")]
        public string Id1 { get; set; }
        [DisplayName("ID2")]
        public string Id2 { get; set; }
        [DisplayName("Key")]
        public string Key { get; set; }
        [DisplayName("Type")]
        public DeviceType Type { get; set; }
        [DisplayName("Status")]
        public DeviceStatus Status { get; set; }
    }

    public class RegisterDeviceVM
    {
        public string Name { get; set; }
        [Required(ErrorMessage ="Id1 required")]
        public string Id1 { get; set; }
        //[Required(ErrorMessage = "Id2 required")]
        public string Id2 { get; set; }
        [Required(ErrorMessage = "Key required")]
        public string Key { get; set; }
        [Required,Range(1,2,ErrorMessage = "Type should be either 1 or 2")]
        public int Type { get; set; }
    }

    public class RegisterDeviceResponseVM
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; } 
    }

    public enum DeviceStatus
    {
        UnRegistered=1,
        Active=2,
        Blocked=3,
        Maintenance=4
    }

    public enum DeviceType
    {
        Desktop=1,
        Mobile=2
    }
}