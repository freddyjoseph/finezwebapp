﻿using FinezWebApp452mvc.Repository.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FinezWebApp452mvc.Models.Context;
using FinezWebApp452mvc.Models;

namespace FinezWebApp452mvc.Repository
{
    public class ClientPackageRepository : Repository<ClientPackage, int>, IClientPackageRepository
    {
        public ClientPackageRepository(ApplicationDbContext _context) : base(_context)
        {
        }

        public override ClientPackage get(int Id)
        {
            return this.getAll().Where(x => x.Id == Id).SingleOrDefault();
        }
    }
}