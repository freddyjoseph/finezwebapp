﻿using FinezWebApp452mvc.Buisiness;
using FinezWebApp452mvc.Controllers.Base;
using FinezWebApp452mvc.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace FinezWebApp452mvc.Controllers
{
    public class BranchController : BaseController
    {
        private BranchManager branchManager { get; set; }
        public BranchController()
        {
            this.branchManager = new BranchManager(this.dbContext);
        }
        // GET: Branch
        public async Task<ActionResult> Index(int Id)
        {
            var branch = await this.branchManager.getBranch(Id);
            if (branch != null)
            {
                return View(branch);
            }
            else
            {
                return View("Error");
            }
        }

        [HttpPut]
        public async Task<ActionResult> Update(EditBranchVM model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await this.branchManager.UpdateBranch(model);
                    ModelState.Clear();
                    model.IsSuccess = true;
                    model.SuccessMessage = "Branch updated successfully";
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Failed to edit branch. Try again");
            }
            return PartialView("EditBranchPartial", model);
        }

        public async Task<ActionResult> GetDevices(int BranchId)
        {
            return PartialView("DeviceListView", await branchManager.GetDevices(BranchId));
        }

        [HttpPost]
        public void GenerateDeviceKey(int BranchId)
        {
            this.branchManager.GenerateAndSaveDeviceKey(BranchId);
        }

        [HttpPut]
        public void UpdateDeviceStatus(int Id,DeviceStatus Status)
        {
            this.branchManager.UpdateDeviceStatus(Id, (int)Status);
        }
    }
}