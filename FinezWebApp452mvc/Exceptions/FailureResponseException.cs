﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinezWebApp452mvc.Exceptions
{
    public class FailureResponseException :Exception
    {
        public FailureResponse FailureResponse = new FailureResponse();

        public FailureResponseException(string _message, string[] _errors)
        {
            this.FailureResponse.Errors = _errors.ToList();
            this.FailureResponse.Message = _message;
        }

        public FailureResponseException(string _message, string _error)
        {
            this.FailureResponse.Errors.Add(_error);
            this.FailureResponse.Message = _message;
        }

        public FailureResponseException()
        {
        }

        public FailureResponseException(string[] _errors)
        {
            this.FailureResponse.Errors = _errors.ToList();
        }

        public FailureResponseException(string _message)
        {
            this.FailureResponse.Message = _message;
        }

        public void addError(string error)
        {
            this.FailureResponse.Errors.Add(error);
        }

        public void setMessage(string message)
        {
            this.FailureResponse.Message = message;
        }

    }

    public class FailureResponse
    {
        public List<String> Errors { get; set; } = new List<string>();
        public string Message { get; set; }
    }
}