﻿using FinezWebApp452mvc.Models.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FinezWebApp452mvc.Models
{
    public class Device:Entity<int>
    {
        [Column("branch_id"),Required]
        [ForeignKey("Branch")]
        public int BranchId { get; set; }
        [Column("name"), MaxLength(100)]
        public string Name { get; set; }
        [Column("id1"),MaxLength(50)]
        public string Id1 { get; set; }
        [Column("id2"), MaxLength(50)]
        public string Id2 { get; set; }
        [Column("key"), Required, MaxLength(50), Index(IsUnique = true)]
        public string Key { get; set; }
        [Column("type")]
        public int Type { get; set; }
        [Column("status")]
        public int Status { get; set; }

        public virtual Branch Branch { get; set; }

    }
}