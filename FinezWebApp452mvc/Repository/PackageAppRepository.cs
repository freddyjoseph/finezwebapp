﻿using FinezWebApp452mvc.Repository.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FinezWebApp452mvc.Models.Context;
using FinezWebApp452mvc.Models;

namespace FinezWebApp452mvc.Repository
{
    public class PackageAppRepository : Repository<PackageApp, int>
    {
        public PackageAppRepository(ApplicationDbContext _context) : base(_context)
        {
        }

        public override PackageApp get(int Id)
        {
            return this.getAll().Where(x => x.Id == Id).SingleOrDefault();
        }
    }
}