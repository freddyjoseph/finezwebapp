﻿using FinezWebApp452mvc.Models.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinezWebApp452mvc.Buisiness.Abstract
{
    public abstract class AbstractManager
    {
        protected ApplicationDbContext context { get; set; }
        public AbstractManager(ApplicationDbContext _context)
        {
            context = _context;
        }
    }
}