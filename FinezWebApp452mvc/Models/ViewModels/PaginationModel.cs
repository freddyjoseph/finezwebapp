﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinezWebApp452mvc.Models.ViewModels
{
    public class PaginationModel
    {
        public int CurrentPage { get; set; }
        public int RowCount { get; set; }
        public int PerPage { get; set; }
        public string OnClickfunction { get; set; }
    }
}