﻿using FinezWebApp452mvc.Buisiness.Abstract;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using FinezWebApp452mvc.Models.Context;
using FinezWebApp452mvc.Repository;
using FinezWebApp452mvc.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;

namespace FinezWebApp452mvc.Buisiness
{
    public class BackupManager : AbstractManager
    {
        IBranchRepository branchRepository;
        IEmployeeRepository employeeRepository;
        public BackupManager(ApplicationDbContext _context) : base(_context)
        {
            branchRepository = new BranchRepository(_context);
            employeeRepository = new EmployeeRepository(_context);
        }

        public void backup(HttpPostedFile file, string fileName, IOwinContext OwinContext)
        {
            ApplicationUser LoggedInUser = OwinContext.GetUserManager<ApplicationUserManager>().FindById(HttpContext.Current.User.Identity.GetUserId());
            var branch = branchRepository.get(employeeRepository.getAll().Where(x => x.UserId == LoggedInUser.Id).Select(y => y.BranchId).Single());

            string path = HttpContext.Current.Server.MapPath("~") 
                + "\\backup\\"+branch.Client.Name+"\\"+branch.Name
                +"\\"+DateTime.Now.ToShortDateString().Replace("\\","-").Replace("/", "-") + "\\";
            Directory.CreateDirectory(path);
            string fullPath = path + fileName; ;
            file.SaveAs(fullPath);
        }
    }
}