﻿using AutoMapper;
using FinezWebApp452mvc.Buisiness.Abstract;
using FinezWebApp452mvc.Models;
using FinezWebApp452mvc.Models.Context;
using FinezWebApp452mvc.Models.ViewModels;
using FinezWebApp452mvc.Repository;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace FinezWebApp452mvc.Buisiness
{
    public class ClientManager:AbstractManager
    {
        IClientRepository clientRepository { get; set; }
        public ClientManager(ApplicationDbContext _context) : base(_context)
        {
            clientRepository = new ClientRepository(this.context);
        }

        public Client addClient(AddClientViewModel model)
        {
            Client client = Mapper.Map<Client>(model);
            client.Status = (int)ClientStatus.Active;
            return clientRepository.insert(client);
        }

        public async Task<SearchClientVM> getClients(SearchClientVM model)
        {
            var clients = clientRepository.getAll()
                .Where(x => model.searchText== null || x.Name.Contains(model.searchText) || x.Phone.Contains(model.searchText)
                || x.Mobile.Contains(model.searchText) || x.Email.Contains(model.searchText));
            model.RowCount = clients.Count();
            if (model.PageNo != 1)
            {
                clients= clients
                .OrderBy(o => o.Id)
                .Skip(model.PageNo - 1 * model.PerPage);
            }
            model.Clients =Mapper.Map<List<ClientVM>>(clients.Take(model.PerPage).ToList());
            return model;
        }

        public async Task<ClientVM> getClient(int id)
        {
            return Mapper.Map<ClientVM>(this.clientRepository.get(id));
        }
    }
}