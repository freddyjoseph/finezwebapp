﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FinezWebApp452mvc.Models.ViewModels
{
    public class AddClientViewModel
    {
        [MaxLength(50), Required]
        public string Name { get; set; }
        [DataType(DataType.PhoneNumber),Phone, MaxLength(50)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string Phone { get; set; }
        [ Required, DataType(DataType.PhoneNumber), Phone, MaxLength(50)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string Mobile { get; set; }
        [ Required, DataType(DataType.EmailAddress),EmailAddress,  MaxLength(50)]
        public string Email { get; set; }
        [MaxLength(6), Required,Display(Name ="Client code")]
        public string ClientCode { get; set; }
        [Display(Name = "Branch count")]
        public int BranchCount { get; set; }
        [Display(Name = "Device count")]
        public int DeviceCount { get; set; }

        public bool IsAdded { get; set; } = false;
        public string SuccessMessage { get; set; } = "";
    }
    
    public class SearchClientVM
    {
        public string searchText { get; set; } = "";
        public int PerPage { get; set; } = 10;
        public int PageNo { get; set; } = 1;
        public int RowCount { get; set; }
        public List<ClientVM> Clients { get; set; }
        public List<SelectListItem> perPageList = new List<SelectListItem>
        {
            new SelectListItem {Text="10",Value="10" },
            new SelectListItem {Text="25",Value="25" },
            new SelectListItem {Text="50",Value="50" },
            new SelectListItem {Text="100",Value="100" },
        };
    }

    public class ClientVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        [Display(Name = "Client code")]
        public string ClientCode { get; set; }
        [Display(Name = "Branch count")]
        public int BranchCount { get; set; }
        [Display(Name = "Device count")]
        public int DeviceCount { get; set; }
        public ClientStatus Status { get; set; }
    }

    public enum ClientStatus
    {
        InActive=0,
        Active=1
    }
}