﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FinezWebApp452mvc.Models.ViewModels
{
    public class AddEmployeeVM
    {
        [MaxLength(50), Required]
        public string Name { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        [DataType(DataType.PhoneNumber),Phone, MaxLength(50)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string Phone { get; set; }
        [Required, DataType(DataType.PhoneNumber),Phone, MaxLength(50)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string Mobile { get; set; }
        [Required, DataType(DataType.EmailAddress),EmailAddress, MaxLength(50)]
        public string Email { get; set; }
        public string Role { get; set; }
        public bool EnableLogin { get; set; } = true;
    }

    public class EmployeeVM
    {
        public int BranchId { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public EmployeeStatus Status { get; set; }
    }

    public class ChangePasswordVM
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
    }
    public enum EmployeeStatus
    {
        Active=1,
        InActive=0
    }
}