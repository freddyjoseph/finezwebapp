//activating current page's menu item in side menu
$("ul.parent-menu > li > a.active").each(function(){
  // checking if small sidebar is shown
  if($(this).closest(".sidebar-sm").length==0){
    //if not show submenu
    $(this).parent().find(".child-menu").show();
  }
});

// click event for side menu wich are not active
$("ul.parent-menu > li > a").bind("click",function() {
  //remove focused state of all other classes
  $(".sidebar li > a").not(this).each(function(){
    $(this).removeClass("focus");
    $(this).parent().find(".child-menu").slideUp();
  });
  // check if selected element is currently active
  if(!$(this).hasClass("active")){
    // if not active , then setting focus
    $(this).hasClass("focus")?$(this).removeClass("focus"):$(this).addClass("focus");
  }
  // $(this).hasClass("focus")?$(this).removeClass("focus"):$(this).addClass("focus");
  $(this).parent().find(".child-menu").slideToggle();
})

// click event for side-bar-toggle button// toggling b/w sidebar and sidebar-sm of target sidebar
$("button.side-bar-toggle").bind("click",function() {
  var target=$(this).attr("data-target").toString();
  // checking has class sidebar-sm//ie. is small sidebar is showing
  if($(target).hasClass("sidebar-sm")){
    // remove sidebar-sm
    $(target).removeClass("sidebar-sm");
    // showing active submenus
    $(target).find("ul.parent-menu > li > a.active + ul.child-menu").show();
  }
  else{
    // add sidebar-sm
    $(target).addClass("sidebar-sm");
    // hiding active submenus
    $(target).find("ul.parent-menu > li > a.active + ul.child-menu").hide();
  }
})

