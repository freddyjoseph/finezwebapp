﻿using FinezWebApp452mvc.Repository.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FinezWebApp452mvc.Models.Context;
using FinezWebApp452mvc.Models;

namespace FinezWebApp452mvc.Repository
{
    public class DeviceRepository : Repository<Device, int>, IDeviceRepository
    {
        public DeviceRepository(ApplicationDbContext _context) : base(_context)
        {
        }

        public override Device get(int Id)
        {
            return this.getAll().Where(x => x.Id == Id).SingleOrDefault();
        }
    }
}