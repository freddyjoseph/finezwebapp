﻿using FinezWebApp452mvc.Models.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FinezWebApp452mvc.Models
{
    public class PackageApp:Entity<int>
    {
        [Column("package_id"),Required]
        [ForeignKey("Package")]
        public int PackageId { get; set; }
        [Column("app_id"), Required]
        [ForeignKey("App")]
        public int AppId { get; set; }

        public virtual Package Package { get; set; }
        public virtual FinezApp App { get; set; }
    }
}