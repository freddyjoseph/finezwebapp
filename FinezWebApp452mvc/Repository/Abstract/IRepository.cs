﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FinezWebApp452mvc.Repository.Abstract
{
    interface IRepository<TEntity,TPrimaryKeyType>
    {
        TEntity get(TPrimaryKeyType Id);
        IQueryable<TEntity> getAll();
        List<TEntity> getAllList();
        TEntity insert(TEntity entity);
        TEntity insertWithNoCommit(TEntity entity);
        void update(TEntity entity);
        void updateWithNoCommit(TEntity entity);
        void delete(TEntity entity);
        void deleteWithNoCommit(TEntity entity);
        void saveChanges();
        DbContextTransaction beginTransaction();
        void commitTransaction();
        void rollbackTransaction();
        int count();
        int count(Expression<Func<TEntity,bool>> predicate);

    }
}
