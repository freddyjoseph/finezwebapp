﻿using FinezWebApp452mvc.Models;
using FinezWebApp452mvc.Models.Context;
using FinezWebApp452mvc.Models.ViewModels;
using FinezWebApp452mvc.Repository;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace FinezWebApp452mvc.Attributes
{
    public class AuthorizeDeviceAttribute: AuthorizeAttribute
    {
        private static string id1Param = "id1", id2Param = "id2", keyParam = "key";
        IDeviceRepository deviceRepository;
        IBranchRepository branchRepository;
        IEmployeeRepository employeeRepository;
        public AuthorizeDeviceAttribute()
        {
            var dbContext = new ApplicationDbContext();
            branchRepository = new BranchRepository(dbContext);
            employeeRepository = new EmployeeRepository(dbContext);
            deviceRepository = new DeviceRepository(dbContext);
        }
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            return AuthorizeDevice(actionContext);
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            base.HandleUnauthorizedRequest(actionContext);
            actionContext.Response.Content = new StringContent("Device authorization failed");
        }

        private bool AuthorizeDevice(HttpActionContext actionContext)
        {
            try
            {
                HttpRequestMessage request = actionContext.Request;
                string id1 = request.Headers.GetValues(id1Param).First();
                //string id2 = request.Headers.GetValues(id2Param).First();
                string key = request.Headers.GetValues(keyParam).First();
                ApplicationUser LoggedInUser = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(HttpContext.Current.User.Identity.GetUserId());
                var branch = branchRepository.get(employeeRepository.getAll().Where(x => x.UserId == LoggedInUser.Id).Select(y => y.BranchId).Single());
                if (this.deviceRepository.getAll().Where(x => x.Id1 == id1 && x.Key == key && x.BranchId == branch.Id && x.Status == (int)DeviceStatus.Active).Any())
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
    }
}