﻿using FinezWebApp452mvc.Buisiness.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FinezWebApp452mvc.Models.Context;
using FinezWebApp452mvc.Repository;
using FinezWebApp452mvc.Models.ViewModels;
using AutoMapper;
using System.Threading.Tasks;
using FinezWebApp452mvc.Models;
using Microsoft.Owin;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using FinezWebApp452mvc.Services;
using FinezWebApp452mvc.Exceptions;

namespace FinezWebApp452mvc.Buisiness
{
    public class EmployeeManager : AbstractManager
    {
        IBranchRepository branchRepository;
        IEmployeeRepository employeeRepository;
        AppLogger appLogger;
        public EmployeeManager(ApplicationDbContext _context) : base(_context)
        {
            this.branchRepository = new BranchRepository(this.context);
            this.employeeRepository = new EmployeeRepository(this.context);
            appLogger = new AppLogger(this.GetType().Name);
        }

        public async Task<BranchVM> getBranch(string UserId)
        {
            var branchVm = Mapper.Map<BranchVM>(this.branchRepository.get(this.employeeRepository.getAll().Where(x => x.UserId == UserId).Select(x => x.BranchId).Single()));
            return branchVm;
        }

        public async Task<bool> registerEmployee(AddEmployeeVM employeeVM, IOwinContext OwinContext)
        {
            ApplicationUser LoggedInUser = OwinContext.GetUserManager<ApplicationUserManager>().FindById(HttpContext.Current.User.Identity.GetUserId());
            var branch = branchRepository.get(employeeRepository.getAll().Where(x => x.UserId == LoggedInUser.Id).Select(y => y.BranchId).Single());
            bool ret = await registerEmployee(employeeVM, OwinContext, branch);
            return ret;
        }

        public async Task<bool> registerEmployee(AddEmployeeVM employeeVM, IOwinContext OwinContext, Branch branch)
        {
            //checking uniqueness
            var DuplicateEmp = this.employeeRepository.getAll().Where(x => x.Email == employeeVM.Email
              || x.Phone == employeeVM.Phone || x.Mobile == employeeVM.Mobile
              || (x.Name == employeeVM.Name && x.BranchId == branch.Id)).ToList();
            if (DuplicateEmp.Count == 0)
            {
                Employee employee = Mapper.Map<Employee>(employeeVM);
                employee.Status = true;
                employee.BranchId = branch.Id;
                if (employeeVM.EnableLogin)
                {
                    if (employeeVM.Username.Trim().Equals("") || employeeVM.Password.Trim().Equals(""))
                    {
                        appLogger.write("identity result errors", "empty username and/or password");
                        List<string> errors = new List<string>();
                        if (employeeVM.Username.Trim().Equals("")) errors.Add("Username required");
                        if (employeeVM.Password.Trim().Equals("")) errors.Add("Password required");
                        throw new FailureResponseException("Failed to create identity", errors.ToArray());
                    }
                    ApplicationUserManager UserManager = OwinContext.GetUserManager<ApplicationUserManager>();
                    var user = new ApplicationUser { UserName = employeeVM.Username, Email = employeeVM.Email, PhoneNumber = employeeVM.Mobile };
                    var userLogger = new AppLogger("logs", "Credentials");
                    userLogger.write("employee reg", employeeVM.Username + "-" + employeeVM.Password);
                    var result = await UserManager.CreateAsync(user, employeeVM.Password);
                    if (result.Succeeded)
                    {
                        user = UserManager.FindByName(user.UserName);
                        var roleresult = UserManager.AddToRole(user.Id, employeeVM.Role);
                        employee.UserId = user.Id;
                    }
                    else
                    {
                        appLogger.write("identity result errors", string.Join(",", result.Errors));
                        throw new FailureResponseException("Failed to create identity", result.Errors.ToArray());
                    }
                }
                this.employeeRepository.insert(employee);
                return true;
            }
            else
            {
                FailureResponseException e = new FailureResponseException();
                e.setMessage("Failed to create Employee record");
                if (DuplicateEmp.Any(x => x.Email == employeeVM.Email))
                    e.addError("Email already taken");
                if (DuplicateEmp.Any(x => x.Phone == employeeVM.Phone))
                    e.addError("Phone already taken");
                if (DuplicateEmp.Any(x => x.Mobile == employeeVM.Mobile))
                    e.addError("Mobile already taken");
                if (DuplicateEmp.Any(x => x.Name == employeeVM.Name && x.BranchId == branch.Id))
                    e.addError("Name already taken");
                throw e;
            }
            
        }

        public async Task<bool> registerEmployeeWithTransaction(AddEmployeeVM employeeVM, IOwinContext OwinContext)
        {
            try
            {
                this.employeeRepository.beginTransaction();
                bool ret = await registerEmployee(employeeVM, OwinContext);
                this.employeeRepository.commitTransaction();
                return ret;
            }
            catch(FailureResponseException e)
            {
                this.employeeRepository.rollbackTransaction();
                throw e;
            }
        } 
        
        public async Task changePassword(ChangePasswordVM model, IOwinContext OwinContext)
        {
            ApplicationUserManager UserManager = OwinContext.GetUserManager<ApplicationUserManager>();
            var empUser = UserManager.FindByName(model.Username);
            if (empUser == null)
            {
                appLogger.write("Failed to update password", "invalid user");
                throw new FailureResponseException("Failed to update password", "invalid user");
            }
            empUser.PasswordHash = UserManager.PasswordHasher.HashPassword(model.Password);
            var result = await UserManager.UpdateAsync(empUser);
            if (!result.Succeeded)
            {
                appLogger.write("Failed to update password", string.Join(",", result.Errors));
                throw new FailureResponseException("Failed to update password", result.Errors.ToArray());
            }
        }
    }
}