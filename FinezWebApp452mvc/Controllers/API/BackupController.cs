﻿using FinezWebApp452mvc.Attributes;
using FinezWebApp452mvc.Buisiness;
using FinezWebApp452mvc.Controllers.Base;
using FinezWebApp452mvc.Models.ViewModels;
using FinezWebApp452mvc.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace FinezWebApp452mvc.Controllers.API
{
    [Authorize]
    public class BackupController : BaseAPIController
    {

        BackupManager backupManager;
        AppLogger appLogger;

        public BackupController()
        {
            backupManager = new BackupManager(this.dbContext);
            appLogger = new AppLogger(this.GetType().Name);
        }

        [AuthorizeDevice]
        public IHttpActionResult index()
        {
            try
            {
                backupManager.backup(HttpContext.Current.Request.Files[0], HttpContext.Current.Request.Form.Get("fileName"), HttpContext.Current.GetOwinContext());
                return Ok();
            }
            catch(Exception e)
            {
                appLogger.write(e);
                return Content(HttpStatusCode.InternalServerError, "Server error");
            }
        }
    }
}
