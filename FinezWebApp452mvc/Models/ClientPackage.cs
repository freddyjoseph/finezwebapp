﻿using FinezWebApp452mvc.Models.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FinezWebApp452mvc.Models
{
    public class ClientPackage:Entity<int>
    {
        [Column("client_id"), Required]
        [ForeignKey("Client")]
        public int ClientId { get; set; }
        [Column("package_id"), Required]
        [ForeignKey("Package")]
        public int PackageId { get; set; }
        [Column("purchased_price"), DataType(DataType.Currency), Required]
        public float PurchasedPrice { get; set; }
        [Column("start_date"),Required]
        public DateTime StartDate { get; set; }
        [Column("end_date"),Required]
        public DateTime EndDate { get; set; }
        [Column("note"), MaxLength(200)]
        public string Note { get; set; }
        [Column("status")]
        public string Status { get; set; }

        public virtual Package Package { get; set; }
        public virtual Client Client { get; set; }
    }
}