﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FinezWebApp452mvc.Models.ViewModels
{
    public class AddBranchVM
    {
        public int ClientId { get; set; }
        [MaxLength(50), Required]
        public string Name { get; set; }
        [Display(Name = "Street")]
        [MaxLength(100), Required]
        public string Street { get; set; }
        [Display(Name ="City")]
        [MaxLength(50), Required]
        public string City { get; set; }
        [Display(Name = "District")]
        [MaxLength(50), Required]
        public string District { get; set; }
        [Display(Name = "State")]
        [MaxLength(50), Required]
        public string State { get; set; }
        [Display(Name = "Pin code")]
        [MaxLength(50), Required]
        public string Pincode { get; set; }
        [DataType(DataType.PhoneNumber),Phone, MaxLength(50)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string Phone { get; set; }
        [Required, DataType(DataType.PhoneNumber), Phone, MaxLength(50)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string Mobile { get; set; }
        [Required, DataType(DataType.EmailAddress),EmailAddress, MaxLength(50)]
        public string Email { get; set; }
        [ DataType(DataType.Url)]
        public string Website { get; set; }
        [Display(Name = "Expiry date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ExpiryDate { get; set; }
        [Display(Name ="Is main branch")]
        public bool IsMainBranch { get; set; }
        public bool IsSuccess { get; set; } = false;
        public string SuccessMessage { get; set; } = "";

        public AddEmployeeVM EmployeeVM { get; set; }
    }

    public class EditBranchVM
    {
        public int Id { get; set; }
        [MaxLength(50), Required]
        public string Name { get; set; }
        [Display(Name = "Street")]
        [MaxLength(100), Required]
        public string Street { get; set; }
        [Display(Name = "City")]
        [MaxLength(50), Required]
        public string City { get; set; }
        [Display(Name = "District")]
        [MaxLength(50), Required]
        public string District { get; set; }
        [Display(Name = "State")]
        [MaxLength(50), Required]
        public string State { get; set; }
        [Display(Name = "Pin code")]
        [MaxLength(50), Required]
        public string Pincode { get; set; }
        [DataType(DataType.PhoneNumber), Phone, MaxLength(50)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string Phone { get; set; }
        [Required, DataType(DataType.PhoneNumber), Phone, MaxLength(50)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string Mobile { get; set; }
        [Required, DataType(DataType.EmailAddress), EmailAddress, MaxLength(50)]
        public string Email { get; set; }
        [DataType(DataType.Url)]
        public string Website { get; set; }
        [Display(Name = "Expiry date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ExpiryDate { get; set; }
        [Display(Name = "Is main branch")]
        public bool IsMainBranch { get; set; }
        public bool IsSuccess { get; set; } = false;
        public string SuccessMessage { get; set; } = "";
    }

    public class BranchVM
    {
        public int BranchId { get; set; }
        public int ClientId { get; set; }
        public string Name { get; set; }
        public string BranchCode { get; set; }
        public string ClientName { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string District { get; set; }
        public string State { get; set; }
        public string Pincode { get; set; }
        [DataType(DataType.PhoneNumber),Phone, MaxLength(50)]
        public string Phone { get; set; }
        [Required, DataType(DataType.PhoneNumber),Phone, MaxLength(50)]
        public string Mobile { get; set; }
        [Required, DataType(DataType.EmailAddress),EmailAddress, MaxLength(50)]
        public string Email { get; set; }
        [DataType(DataType.Url)]
        public string Website { get; set; }
        public bool IsMainBranch { get; set; }
        public DateTime LastUpdatedDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ExpiryDate { get; set; }
        public List<EmployeeVM> Employees { get; set; }

        public BranchStatus Status { get; set; }
    }

    public enum BranchStatus
    {
        InActive = 0,
        Active = 1
    }
}