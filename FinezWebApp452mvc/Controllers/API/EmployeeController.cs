﻿using FinezWebApp452mvc.Attributes;
using FinezWebApp452mvc.Buisiness;
using FinezWebApp452mvc.Controllers.Base;
using FinezWebApp452mvc.Exceptions;
using FinezWebApp452mvc.Models;
using FinezWebApp452mvc.Models.ViewModels;
using FinezWebApp452mvc.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace FinezWebApp452mvc.Controllers.API
{
    public class EmployeeController : BaseAPIController
    {
        EmployeeManager employeeManager;
        AppLogger appLogger;
        public EmployeeController()
        {
            this.employeeManager = new EmployeeManager(this.dbContext);
            appLogger = new AppLogger(this.GetType().Name);
        }

        [Authorize]
        public async Task<IHttpActionResult> GetBranch()
        {
            try
            {
                ApplicationUser User = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
                return Ok(await this.employeeManager.getBranch(User.Id));
            }
            catch (Exception e)
            {
                appLogger.write(e);
                return Content(HttpStatusCode.NotFound, new FailureResponse() { Message = "Employee not found" });
            }
        }

        [Authorize(Roles ="BranchAdmin")]
        [AuthorizeDevice]
        [HttpPost]
        public async Task<IHttpActionResult> Register(AddEmployeeVM model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if(await this.employeeManager.registerEmployeeWithTransaction(model, HttpContext.Current.GetOwinContext()))
                    return Ok();
                    else return Content(HttpStatusCode.BadRequest, "Identity error");

                }
                else
                {
                    appLogger.write("Register model error",string.Join(",",ModelState.Values.SelectMany(v => v.Errors)));
                    return Content(HttpStatusCode.BadRequest,new FailureResponse() {Message="Invalid employee details", Errors= ModelState.Values.SelectMany(v => v.Errors.Select(y => y.ErrorMessage)).ToList() } );
                }
            }
            catch (FailureResponseException e)
            {
                appLogger.write(e);
                return Content(HttpStatusCode.BadRequest,e.FailureResponse);
            }
            catch (Exception e)
            {
                appLogger.write(e);
                return Content(HttpStatusCode.InternalServerError, "Server error");
            }
        }

        [Authorize(Roles = "BranchAdmin")]
        [AuthorizeDevice]
        [HttpPut]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordVM model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await this.employeeManager.changePassword(model, HttpContext.Current.GetOwinContext());
                    return Ok();
                }
                else
                {
                    appLogger.write("Register model error", string.Join(",", ModelState.Values.SelectMany(v => v.Errors)));
                    return Content(HttpStatusCode.BadRequest, new FailureResponse() { Message = "Invalid employee details", Errors = ModelState.Values.SelectMany(v => v.Errors.Select(y => y.ErrorMessage)).ToList() });
                }
            }
            catch (FailureResponseException e)
            {
                appLogger.write(e);
                return Content(HttpStatusCode.BadRequest, e.FailureResponse);
            }
            catch (Exception e)
            {
                appLogger.write(e);
                return Content(HttpStatusCode.InternalServerError, "Server error");
            }
        }
    }
}
