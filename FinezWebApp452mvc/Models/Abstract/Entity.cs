﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FinezWebApp452mvc.Models.Abstract
{
    public abstract class Entity<TType>
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Column("id")]
        public TType Id { get; set; }
        [Column("created_user")]
        [MaxLength(50)]
        public virtual string CreatedUser { get; set; }
        [Column("created_date",TypeName = "datetime2")]
        public virtual DateTime? CreatedDate { get; set; }
        [Column("modified_user")]
        [MaxLength(50)]
        public virtual string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime2")]
        public virtual DateTime? ModifiedDate { get; set; }
    }
}