﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FinezWebApp452mvc.Models
{
    public class Client: Abstract.Entity<int>
    {
        [Column("name")]
        [MaxLength(50),Required]
        public string Name { get; set; }
        [Column("phone"),DataType(DataType.PhoneNumber),Index(IsUnique =true),MaxLength(50)]
        public string Phone { get; set; }
        [Column("mobile"),Required,DataType(DataType.PhoneNumber), Index(IsUnique = true), MaxLength(50)]
        public string Mobile { get; set; }
        [Column("email"),Required,DataType(DataType.EmailAddress), Index(IsUnique = true), MaxLength(50)]
        public string Email { get; set; }
        [Column("client_code")]
        [MaxLength(50), Required]
        public string ClientCode { get; set; }
        [Column("branch_count"), Required]
        public int BranchCount { get; set; }
        [Column("device_count"), Required]
        public int DeviceCount { get; set; }
        [Column("status")]
        public int Status { get; set; }
        
        public virtual ICollection<Branch> Branches { get; set; }
        public virtual ICollection<ClientPackage> ClientPackages { get; set; }
    }
}