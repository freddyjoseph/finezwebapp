﻿using FinezWebApp452mvc.Models.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FinezWebApp452mvc.Models
{
    [Table("PackageMaster")]
    public class Package:Entity<int>
    {
        [Column("name"),Required]
        public string Name { get; set; }
        [Column("price"),DataType(DataType.Currency),Required]
        public float Price { get; set; }
        [Column("validity")]
        public int Validity { get; set; }

        public virtual ICollection<PackageApp> PackageApps { get; set; }
        public virtual ICollection<ClientPackage> ClientPackages { get; set; }
    }
}