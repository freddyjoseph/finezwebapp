﻿using FinezWebApp452mvc.Models.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FinezWebApp452mvc.Models
{
    public class Employee:Entity<int>
    {
        [Column("branch_id"), Required]
        [ForeignKey("Branch")]
        public int BranchId { get; set; }
        [Column("UserId", TypeName = "nvarchar"), ForeignKey("User"), MaxLength(128)]
        public string UserId { get; set; }
        [Column("name")]
        [MaxLength(50), Required]
        public string Name { get; set; }
        [Column("phone"), DataType(DataType.PhoneNumber), Index(IsUnique = true), MaxLength(50)]
        public string Phone { get; set; }
        [Column("mobile"), Required, DataType(DataType.PhoneNumber), Index(IsUnique = true), MaxLength(50)]
        public string Mobile { get; set; }
        [Column("email"), Required, DataType(DataType.EmailAddress), Index(IsUnique = true), MaxLength(50)]
        public string Email { get; set; }
        [Column("status")]
        public bool Status { get; set; }

        public virtual Branch Branch { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}