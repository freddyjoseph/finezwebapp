﻿using FinezWebApp452mvc.Models;
using FinezWebApp452mvc.Repository.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinezWebApp452mvc.Repository
{
    interface IPackageAppRepository:IRepository<PackageApp,int>
    {
    }
}