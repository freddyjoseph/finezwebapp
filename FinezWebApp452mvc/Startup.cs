﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FinezWebApp452mvc.Startup))]
namespace FinezWebApp452mvc
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
