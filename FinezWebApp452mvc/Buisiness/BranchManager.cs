﻿using FinezWebApp452mvc.Buisiness.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FinezWebApp452mvc.Models.Context;
using FinezWebApp452mvc.Repository;
using FinezWebApp452mvc.Models.ViewModels;
using System.Threading.Tasks;
using Microsoft.Owin;
using Microsoft.AspNet.Identity.Owin;
using FinezWebApp452mvc.Models;
using Microsoft.AspNet.Identity;
using AutoMapper;
using FinezWebApp452mvc.Services;
using FinezWebApp452mvc.Exceptions;

namespace FinezWebApp452mvc.Buisiness
{
    public class BranchManager : AbstractManager
    {
        IBranchRepository branchRepository;
        IEmployeeRepository employeeRepository;
        IDeviceRepository deviceRepository;
        AppLogger appLogger;
        EmployeeManager employeeManager;
        ClientManager clientManager;
        public BranchManager(ApplicationDbContext _context) : base(_context)
        {
            branchRepository = new BranchRepository(this.context);
            this.employeeRepository = new EmployeeRepository(this.context);
            this.deviceRepository = new DeviceRepository(this.context);
            appLogger = new AppLogger(this.GetType().Name);
            employeeManager = new EmployeeManager(this.context);
            clientManager = new ClientManager(this.context);
        }

        public async Task<AddBranchVM> AddBranchAndAdmin(AddBranchVM model, IOwinContext OwinContext)
        {
            try
            {
                this.branchRepository.beginTransaction();
                ClientVM client =await clientManager.getClient(model.ClientId);
                Branch branch = Mapper.Map<Branch>(model);
                branch.Status = (int)BranchStatus.Active;
                branch.BranchCode = client.ClientCode + (this.branchRepository.count(x => x.ClientId == model.ClientId)+1);
                this.branchRepository.insert(branch);
                model.EmployeeVM.Role = Roles.BranchAdmin.ToString();
                //creating username and password
                model.EmployeeVM.Username = (branch.BranchCode + "_" + model.EmployeeVM.Name).Replace(" ", "");
                model.EmployeeVM.Password = System.Web.Security.Membership.GeneratePassword(12, 2);
                if (await this.employeeManager.registerEmployee(model.EmployeeVM, OwinContext, branch))
                {
                    model = new AddBranchVM();
                    model.IsSuccess = true;
                    model.SuccessMessage = "Branch added successfully";
                    this.branchRepository.commitTransaction();
                }
                else
                {
                    model.IsSuccess = false;
                    this.branchRepository.rollbackTransaction();
                }
            }
            catch(Exception e)
            {
                model.IsSuccess = false;
                appLogger.write(e);
                this.branchRepository.rollbackTransaction();
                throw e;
            }
            return model;
        }

        public async Task UpdateBranch(EditBranchVM model)
        {
            var branch = this.branchRepository.get(model.Id);
            Mapper.Map(model, branch, typeof(EditBranchVM), typeof(Branch));
            this.branchRepository.update(branch);
        }

        public async Task<BranchVM> getBranch(int Id)
        {
            return Mapper.Map<BranchVM>(this.branchRepository.get(Id));
        }

        public async Task<List<BranchVM>> GetBranches(int ClientId,string SearchText)
        {
            return Mapper.Map<List<BranchVM>>(this.branchRepository.getAll().Where(x => x.ClientId == ClientId && x.Name.Contains(SearchText)).ToList());
        }

        public async Task<List<DeviceVM>> GetDevices(int BranchId)
        {
            return Mapper.Map<List<DeviceVM>>(this.deviceRepository.getAll().Where(x => x.BranchId == BranchId).ToList());
        }

        public void GenerateAndSaveDeviceKey(int BranchId)
        {
            Device device = new Device();
            device.BranchId = BranchId;
            device.Key = StringUtils.RandomString(4) + "-" + StringUtils.RandomString(4) + "-" + StringUtils.RandomString(4) + "-" + StringUtils.RandomString(4);
            device.Status = (int)DeviceStatus.UnRegistered;
            this.deviceRepository.insert(device);
        }

        public async Task RegisterDevice(RegisterDeviceVM model,IOwinContext OwinContext)
        {
            Device device;
            ApplicationUser LoggedInUser = OwinContext.GetUserManager<ApplicationUserManager>().FindById(HttpContext.Current.User.Identity.GetUserId());
            var branch = branchRepository.get(employeeRepository.getAll().Where(x => x.UserId == LoggedInUser.Id).Select(y => y.BranchId).Single());
            if(this.deviceRepository.getAll().Where(x => x.Key==model.Key && x.Id1 == model.Id1 && x.Id2 == model.Id2 && x.Status == (int)DeviceStatus.Maintenance).Any())
            {
                device = this.deviceRepository.getAll().Where(x => x.Key == model.Key && x.Id1 == model.Id1 && x.Id2 == model.Id2 && x.Status == (int)DeviceStatus.Maintenance).Single();
                device.Status = (int)DeviceStatus.Active;
                this.deviceRepository.saveChanges();
                return;
            }
            if(this.deviceRepository.getAll().Where(x=>x.Id1==model.Id1 && x.Id2==model.Id2 && x.Status != (int)DeviceStatus.Blocked).Any())
            {
                throw new FailureResponseException("Device registration failed",new string[] { "Device Ids are already used" });
            }
            if (this.deviceRepository.getAll().Where(x => x.Key == model.Key && x.Status != (int)DeviceStatus.UnRegistered).Any())
            {
                throw new FailureResponseException("Device registration failed", new string[] { "Key is already taken" });
            }
            device = this.deviceRepository.getAll().Where(x=>x.Key==model.Key && x.BranchId==branch.Id).SingleOrDefault();
            if (device == null)
            {
                throw new FailureResponseException("Device registration failed", new string[] { "Invalid key" });
            }
            device.Name = model.Name;
            device.Id1 = model.Id1;
            device.Id2 = model.Id2;
            device.BranchId = branch.Id;
            device.Status = (int)DeviceStatus.Active;
            this.deviceRepository.saveChanges();
            return;
        }

        public async Task UpdateDeviceStatus(int id,int status)
        {
            Device device= this.deviceRepository.get(id);
            if (device == null)
            {
                throw new FailureResponseException("Device updation failed", new string[] { "Device not found" });
            }
            device.Status = status;
            this.deviceRepository.saveChanges();
        }
    }
}