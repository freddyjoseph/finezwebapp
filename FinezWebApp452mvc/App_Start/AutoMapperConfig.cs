﻿using AutoMapper;
using FinezWebApp452mvc.Models;
using FinezWebApp452mvc.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinezWebApp452mvc.App_Start
{
    public class AutoMapperConfig
    {
        public static void initialize()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<AddClientViewModel, Client>();
                cfg.CreateMap<Client, ClientVM>().ForMember(d => d.Status, o => o.MapFrom(s => (ClientStatus)s.Status));
                cfg.CreateMap<AddBranchVM, Branch>();
                cfg.CreateMap<AddEmployeeVM, Employee>();
                cfg.CreateMap<Employee, EmployeeVM>()
                .ForMember(d=>d.Status,o=>o.MapFrom(s=>s.Status? EmployeeStatus.Active: EmployeeStatus.InActive));
                cfg.CreateMap<Branch, BranchVM>()
                .ForMember(d => d.BranchId, o => o.MapFrom(s => s.Id))
                .ForMember(d=>d.LastUpdatedDate,o=>o.MapFrom(s=>s.ModifiedDate))
                .ForMember(d => d.ClientName, o => o.MapFrom(s => s.Client.Name));
                cfg.CreateMap<BranchVM, EditBranchVM>();
                cfg.CreateMap<EditBranchVM, Branch>();
                cfg.CreateMap<Device, DeviceVM>()
                .ForMember(d => d.Status, o => o.MapFrom(s => (DeviceStatus)s.Status))
                .ForMember(d => d.Type, o => o.MapFrom(s => (DeviceType)s.Type));
                cfg.CreateMap<RegisterDeviceVM, Device>();
            });
        }
    }
}