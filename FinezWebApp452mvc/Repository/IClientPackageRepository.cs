﻿using FinezWebApp452mvc.Models;
using FinezWebApp452mvc.Repository.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinezWebApp452mvc.Repository
{
    interface IClientPackageRepository:IRepository<ClientPackage,int>
    {
    }
}
