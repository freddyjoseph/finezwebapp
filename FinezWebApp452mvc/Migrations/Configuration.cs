namespace FinezWebApp452mvc.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<FinezWebApp452mvc.Models.Context.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(FinezWebApp452mvc.Models.Context.ApplicationDbContext context)
        {
            context.Packages.AddOrUpdate(x => x.Id,
                new Models.Package()
                { Id = 1, Name = "Trial", Price = 0, Validity = 30, CreatedDate = DateTime.Now, ModifiedDate = DateTime.Now, CreatedUser = "System", ModifiedUser = "System" }
                );
            context.FinezApps.AddOrUpdate(x => x.Id,
                new Models.FinezApp()
                { Id = 1, Name = "Lumino", Price = 0, CreatedDate = DateTime.Now, ModifiedDate = DateTime.Now, CreatedUser = "System", ModifiedUser = "System" }
                );
            context.PackageApps.AddOrUpdate(x => x.Id,
                new Models.PackageApp()
                { Id = 1, AppId = 1, PackageId = 1, CreatedDate = DateTime.Now, ModifiedDate = DateTime.Now, CreatedUser = "System", ModifiedUser = "System" }
                );
            base.Seed(context);
        }
    }
}
