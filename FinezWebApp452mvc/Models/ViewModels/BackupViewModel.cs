﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinezWebApp452mvc.Models.ViewModels
{
    public class FileModel
    {
        public byte[] FileData { get; set; }
        public string FileName { get; set; }
    }
}