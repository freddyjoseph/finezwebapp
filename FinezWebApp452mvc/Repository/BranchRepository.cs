﻿using FinezWebApp452mvc.Repository.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FinezWebApp452mvc.Models.Context;
using FinezWebApp452mvc.Models;

namespace FinezWebApp452mvc.Repository
{
    public class BranchRepository : Repository<Branch, int>, IBranchRepository
    {
        public BranchRepository(ApplicationDbContext _context) : base(_context)
        {
        }

        public override Branch get(int Id)
        {
            return this.getAll().Where(x => x.Id == Id).SingleOrDefault();
        }
    }
}