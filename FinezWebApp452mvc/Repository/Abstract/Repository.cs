﻿using FinezWebApp452mvc.Models.Abstract;
using FinezWebApp452mvc.Models.Context;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace FinezWebApp452mvc.Repository.Abstract
{
    public abstract class Repository<TEntity, TPrimaryKeyType> : IRepository<TEntity, TPrimaryKeyType> where TEntity : Entity<TPrimaryKeyType>
    {
        protected ApplicationDbContext context;
        private readonly IDbSet<TEntity> entitySet;
        protected DbContextTransaction transaction;
        public Repository(ApplicationDbContext _context)
        {
            this.context = _context;
            this.entitySet =this.context.Set<TEntity>();
        }
        public void saveChanges()
        {
            this.context.SaveChanges();
        }

        public void delete(TEntity entity)
        {
            this.deleteWithNoCommit(entity);
            this.context.SaveChanges();
        }

        public void deleteWithNoCommit(TEntity entity)
        {
            this.entitySet.Remove(entity);
        }

        public abstract TEntity get(TPrimaryKeyType Id);

        public IQueryable<TEntity> getAll() {
            return this.entitySet;
        }

        public List<TEntity> getAllList()
        {
            return this.getAll().ToList();
        }

        public TEntity insert(TEntity entity)
        {
            this.insertWithNoCommit(entity);
            this.context.SaveChanges();
            return entity;
        }

        public TEntity insertWithNoCommit(TEntity entity)
        {
            entity.CreatedUser=entity.ModifiedUser= HttpContext.Current.User.Identity.GetUserId();
            entity.CreatedDate = entity.ModifiedDate = DateTime.Now;
            return this.entitySet.Add(entity);
        }

        public void update(TEntity entity)
        {
            this.updateWithNoCommit(entity);
            this.context.SaveChanges();
        }

        public void updateWithNoCommit(TEntity entity)
        {
            entity.ModifiedUser = HttpContext.Current.User.Identity.GetUserId();
            entity.ModifiedDate = DateTime.Now;
            this.context.Entry<TEntity>(entity).State=EntityState.Modified;
        }

        public DbContextTransaction beginTransaction()
        {
           return this.transaction = this.context.Database.BeginTransaction();
        }

        public void commitTransaction()
        {
            this.transaction.Commit();
        }

        public void rollbackTransaction()
        {
            this.transaction.Rollback();
        }

        public int count()
        {
            return this.entitySet.Count();
        }

        public int count(Expression<Func<TEntity, bool>> predicate)
        {
            return this.entitySet.Where(predicate).Count();
        }
    }
}